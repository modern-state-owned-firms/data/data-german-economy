Research project

# 'Modern state-owned firms' performance: An empirical analysis of productivity, market power, and innovation'
financed by the German Science Foundation (DFG) | 2019-2022

Author of the following codes: Caroline Stiel (@cjstiel)

All codes here provided are open source. All codes are licenced under the [MIT License](https://spdx.org/licenses/MIT.html) expanded by the [Commons Clause](https://commonsclause.com/). All data sets are licenced under the [Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/).

## Data preparation: EXTERNAL DATA Part I - Data on the German economy

In this step, we prepare external data on the German economy from various sources (e.g., national accounts, Bundesbank, Federal Network Agency) to merge them with the AFiD data. 

**First block: Data from the national accounts**

------------------------------------------------

The German national accounts _(Volkswirtschaftliche Gesamtrechnung)_ gather various data on price indexes, asset lifecycles and the aggregated capital stock of industries at the NACE Rev.2 2-digit and 1-digit industry level.

<details><summary>Click here to see a full list of the variables that we use.</summary>

| variable | description | source |
| ------ | ------ | ------ |
| L_asset | average service life of assets from current investment year | VGR domestic product 2019 |
| L_build | average service life of buildings from current investment year | VGR domestic product 2019 |
| L_equip | average service life of equipment from current investment year | VGR domestic product 2019 |
| L_other | average service life of intellectual property from current investment year | VGR domestic product 2019 |
| CS_asset | net (fixed) assets evaluated at replacement costs | VGR domestic product 2019 |
| CS_equip | net assets (equipment) evaluated at replacement costs | VGR domestic product 2019 |
| CS_build | net assets (buildings) evaluated at replacement costs | VGR domestic product 2019 |
| CS_other | net assets (buildings) evaluated at replacement costs | VGR domestic product 2019 |					
| I_GEI_VGR	| price index gross investments into intellectual property (base year = 2010) |	VGR domestic product 2019 |		
| I_SOFTP_VGR | price index gross investments into software and data bases (base year = 2010) | VGR domestic product 2019 |	| I_RDP_VGR	| price index gross investments into R&D (base year = 2010) | VGR domestic product 2019 |
| BAV_GEI_VGR | price index gross assets intellectual property (base year = 2010) |	 VGR domestic product 2019 |			
| BAV_SOFTP_VGR	| price index gross assets software and databases (base year = 2010) | VGR domestic product 2019 |			
| BAV_RDP_VGR | price index gross assets R&D (base year = 2010) | VGR domestic product 2019 |
| NAV_GEI_VGR | price index net assets intellectual property (base year = 2010) | VGR domestic product 2019 |
| NAV_SOFTP_VGR | price index net assets software and databases (base year = 2010) | VGR domestic product 2019 |			| NAV_RDP_VGR | price index net assets R&D (base year = 2010) | VGR domestic product 2019 |						
| BAV_VGR | price index total gross assets (base year = 2010) | VGR domestic product 2019 |					
| BAV_AUS_VGR | price index gross assets equipment (base year = 2010) | VGR domestic product 2019 |
| BAV_BAU_VGR | price index gross assets buildings (base year = 2010) | VGR domestic product 2019 |
| BAV_SON_VGR | price index gross assets others (base year = 2010) | VGR domestic product 2019 |	
| KL_ratio_nom_VGR | capital intensity: ratio between total gross assets (nominal) and employees | VGR domestic product 2019 |	
| KL_ratio_real_VGR | capital intensity: ratio between total gross assets (real) and employees | VGR domestic product 2019 |	
| K_DEPR_VGR | depreciation rate of total gross assets  | VGR domestic product 2019 |
| I_TP_VGR | price index total gross investments (base year = 2010) | VGR domestic product 2019 |
| I_AUS_VGR | price index gross investments into equipment  (base year = 2010) | VGR domestic product 2019 |			| I_BAU_VGR | price index gross investments into buildings  (base year = 2010) | VGR domestic product 2019 			| I_SON_VGR | price index gross investments into other assets  (base year = 2010) | VGR domestic product 2019 |		| MP_VGR	| price index intermediate gooods and services  (base year = 2010) | VGR domestic product 2019 |
| GOP_VGR | price index gross production value  (base year = 2010) | VGR domestic product 2019 |
| VAP_VGR | price index value added  (base year = 2010) | VGR domestic product 2019 |
| LCOST_VGR | price index wage costs per hour worked  (base year = 2010) | VGR domestic product 2019 |

Notes: The database	contains the information both at the NACE 1-digit (variable names ending with `_1digit`) and NACE 2-digit level (variable names listed above).
</details>

**Second block: Data from the Federal Ministry for Economic Affairs _(BMWi)_**

------------------------------------------------

The Federal Ministry for Economic Affairs _(BMWi)_ publishes data on the energy and water markets, including domestic production, consumption, prices, and import/export volumes.

<details><summary>Click here to see a full list of the variables that we use.</summary>

| variable | description | source |
| ------ | ------ | ------ |
| Strompreis_Ind_ct_kWh | consumer prices of industry sector: electricity \[ct/kWh\] | BMWi (2020): Energiedaten. |
| Heizoelpreis_l_Ind_EUR_hl | consumer prices of industry sector: light fuel oil [EUR/hl\] | BMWi (2020): Energiedaten. |
|  Heizoelpreis_s_Ind_EUR_t | consumer prices of industry sector: heavy fuel oil [EUR/t\] | BMWi (2020): Energiedaten. |
| Erdgaspreis_Ind_ct_kWh | consumer prices of industry sector: Erdgas [ct/kWh\] | BMWi (2020): Energiedaten. |
| FW_HH_EUR_GJ | consumer prices of households: district heat [EUR/GJ\] | BMWi (2020): Energiedaten. |
| Erdgaspreis_HH_ct_kWh |  consumer prices of households: natural gas [ct/kWh\] | BMWi (2020): Energiedaten. |
|  Heizoelpreis_HH_EUR_100l	| consumer prices of households: fuel oil [EUR/100l\] | BMWi (2020): Energiedaten. |
| Strompreis_HH_ct_kWh | consumer prices of households: electricity [ct/kWh\] | BMWi (2020): Energiedaten. |
| Steinkohle_Import_EUR_t_SKE | import prices: hard coal [EUR/t_SKE\] | BMWi (2020): Energiedaten. |
| Erdgas_Import_EUR_TJ | import prices: natural gas [EUR/TJ\] | BMWi (2020): Energiedaten. |
| Rohoel_Import_EUR_t | import prices: crude oil [EUR/t\] | BMWi (2020): Energiedaten. |
</details>

**Third block: Data from the Federal Statistical Office _(Destatis)_**

-----------------------------------------------------------

The Federal Statistical Office _(Destatis)_ publishes further granular data on energy prices by type of production or consumption in their GENESIS database.

<details><summary>Click here to see a full list of the variables that we use.</summary>

| variable | description | source |
| ------ | ------ | ------ |
| Erdgas_defl_BJ2015 | producer price index for natural gas, liquefied or gaseous (baseyear 2015=100) | GENESIS code 61241-0003 |
| Erdoel_defl_BJ2015 | price index for crude oil from bituminous minerals (baseyear 2015=100)|GENESIS code 61241-0003 |
| Braunkohle_defl_BJ2015 | price index for lignite (baseyear 2015=100)| GENESIS code 61241-0003 |
| Heizoel_l_HH_defl_BJ2015 | price index for light fuel oil for household consumption (baseyear 2015=100) | GENESIS code 61241-0003 |
| Heizoel_s_defl_BJ2010 | producer price index for heavy fuel oil (baseyear 2015=100) | GENESIS code 61241-0003 |
| Strom_defl_BJ2015 | price index for electricity (baseyear 2015=100) | GENESIS code 61241-0003 |
| Strom_Boerse_defl_BJ2015 | price index for electricity listed on the stock exchange (baseyear 2015=100) | GENESIS code 61241-0003 |
| Strom_Transm_defl_BJ2015 | price index for electricity transmission services (baseyear 2015=100) | GENESIS code 61241-0003 |
| Strom_Vtlg_defl_BJ2015 | price index for electricity distribution services (baseyear 2015=100) | GENESIS code 61241-0003 |
| Strom_Trade_defl_BJ2015 | price index for electricity trading services (baseyear 2015=100) | GENESIS code 61241-0003 |
| Strom_WVtlg_defl_BJ2015 | price index for electricity re-distributors (baseyear 2015=100)| GENESIS code 61241-0003 |
| Strom_HH_defl_BJ2015 | price index for electricity for household consumption (baseyear 2015=100) | GENESIS code 61241-0003 |
| Strom_Gewerbe_defl_BJ2015 | price index for electricity for commercial use (baseyear 2015=100) | GENESIS code 61241-0003 |
| Strom_SK_NS_defl_BJ2015 | price index for electricity for special-contract customers in low-voltage tariffs (baseyear 2015=100) | GENESIS code 61241-0003 |
| Strom_SK_HS_defl_BJ2015 | price index for electricity for special-contract customers in high-voltage tariffs (baseyear 2015=100) | GENESIS code 61241-0003 |
| FW_defl_BJ2015 | price index for district heat (baseyear 2015=100)| GENESIS code 61241-0003 |
| FW_MFH_defl_BJ2015 | price index for district heat, apartment building (baseyear 2015=100)| GENESIS code 61241-0003 |
| FW_NWG_defl_BJ2015 | price index for district heat, non-residential building (baseyear 2015=100)| GENESIS code 61241-0003 |
| Erdgas_all_defl_BJ2015 | price index for natural gas transmission, distribution and trade services (baseyear 2015=100)| GENESIS code 61241-0003 |
| Erdgas_HH_defl_BJ2015 | price index for natural gas for household consumption (baseyear 2015=100)| GENESIS code 61241-0003 |
| Erdgas_Ind_defl_BJ2015 | price index for natural gas for manufacturing (baseyear 2015=100)| GENESIS code 61241-0003 |
| Erdgas_HuG_defl_BJ2015 | price index for natural gas for trade and commercial use (baseyear 2015=100)| GENESIS code 61241-0003 |
| Erdgas_KW_defl_BJ2015 | price index for natural gas for power plants (baseyear 2015=100)| GENESIS code 61241-0003 |
| Erdgas_WVtlg_defl_BJ2015 | price index for natural gas for re-distributors (baseyear 2015=100)| GENESIS code 61241-0003 |
| Erdgas_Boerse_defl_BJ2015 | price index for natural gas listed on stock markets (baseyear 2015=100)| GENESIS code 61241-0003 |
| Wa_defl_BJ2015 | price index for water supply (baseyear 2015=100)| GENESIS code 61241-0003 |
| Wa_HH_defl_BJ2015 | price index for water for household consumption (10m3 per month) (baseyear 2015=100)| GENESIS code 61241-0003 |
| Wa_Ind_5000_BJ2015 | price index for water supplied to manufacturing (5000m3 per month) (baseyear 2015=100)| GENESIS code 61241-0003 |
| Wa_Ind_50000_BJ2015 | price index for water supplied to manufacturing (50000m3 per month) (baseyear 2015=100)| GENESIS code 61241-0003 |
| Wa_WVU_BJ2015 | price index for water supplied to re-distributors (baseyear 2015=100)| GENESIS code 61241-0003 |
| Steinkohle_Import_defl_BJ2015 | import price index for hard coal (baseyear 2015=100)| GENESIS code 61411-0005 |
| Erdgas_Import_defl_BJ2015	| import price index for natural gas (baseyear 2015=100)| GENESIS code 61411-0005 |
| CPI_VGR | consumer price index (all products, base year = 2010) | GENESIS code 61111-0001 |
</details>


**Fourth block: Data from the Federal Network Agency _(BNetzA)_**

------------------------------------------------

The Federal Network Agency _(BNetzA)_ collects regulatory data on DSOs and TSOs and publishes standardized parameters for efficiency calculations. 

We use the following variables.

| variable | description | source |
| ------ | ------ | ------ |
| erp  | equity risk premium for electricity DSOs | BNetzA ruling BK4-08-068 | 
| beta | individual risk factor for capital asset pricing model | BNetzA ruling BK4-08-068 |


**Fifth block: Data from the German _Bundesbank_**

------------------------------------------------

The German _Bundesbank_ collects various information on financial markets, monetary policy, and firm data. We use the following variables.

| variable | description | source |
| ------ | ------ | ------ |
| HRefEZB  | Main refinancing rate of the ECB (yearly average) | Bundesbank data base code BBK01.SU0202 |
| Umlaufrendite_Gesamt | yields on bonds outstanding (all bearer bonds, yearly average) | Bundesbank data base code BBSIS.M.I.UMR.RD.EUR.A.B.A.A.R.A.A._Z._Z.A |
| Umlaufrendite_Staat | yields on bonds outstanding (government bonds, yearly average) | Bundesbank data base code BBSIS.M.I.UMR.RD.EUR.S13.B.A.A.R.A.A._Z._Z.A |
| EKQ | equity ratio (equity in % of total assets) | Bundesbank (2020): Hochgerechnete Angaben aus Jahresabschlüssen deutscher Unternehmen von 1995 bis 2018. |
</details>

