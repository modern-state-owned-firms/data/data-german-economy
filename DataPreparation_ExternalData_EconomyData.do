﻿********************************************************************************
*
*  				Research project
*
*			'Modern state-owned firms' performance
* 	An empirical analysis of productivity, market power, and innovation'
* 				(2019-2022)
*
* 		financed by the German Science Foundation (DFG)
*
*  			Data preparation: EXTERNAL DATA I
*
*			- Data on the German economy -
*				
*
********************************************************************************
* Authors: Caroline Stiel (cs)
* Date: 23-08-2021
********************************************************************************


********************************************************************************
* 				0) Global settings
********************************************************************************

* clear workspace
* ---------------
clear
set more off
program drop _all
capture clear matrix
capture log close

* main directory (add path for each user)
* ---------------------------------------
global cs 1 

if $cs==1{
	global MAINDIR "<insert your local working directory>"
}
 
* define dates and time etc. 
* --------------------------
local date=ltrim("$S_DATE")
local date=subinstr("`date'"," ","_",2)

* version number
* --------------
local VERSION "V1"

* close open log file
* -------------------
cap log close

* start recording protocol
* ------------------------
log using "$MAINDIR\LOG_preparation_eonomy_data_`VERSION'_`date'.log", replace


********************************************************************************
* 				START
********************************************************************************

********************************************************************************
*			1) Load energy price data
********************************************************************************

/* Source:
BMWi (Federal Ministry for Economic Affairs): Energiedaten.
Destatis: Daten zur Energiepreisentwicklung. Lange Reihen. 2005-2020.
Destatis (GENESIS database): Erzeugerpreisindex gewerblicher Produkte GP09.
*/
 
* load energy price data
* ----------------------
import excel "$MAINDIR\Energy price data\Energy_price_data_2003-2019.xlsx", ///
sheet("Data") firstrow clear

drop AS AT AU AV AW AX

label variable Strompreis_Ind_ct_kWh "consumer prices of industry sector: electricity [ct/kWh]"
label variable Heizoelpreis_l_Ind_EUR_hl "consumer prices of industry sector: light fuel oil [EUR/hl]"
label variable Heizoelpreis_s_Ind_EUR_t "consumer prices of industry sector: heavy fuel oil [EUR/t]"
label variable Erdgaspreis_Ind_ct_kWh "consumer prices of industry sector: Erdgas [ct/kWh]"
label variable FW_HH_EUR_GJ "consumer prices of households: district heat [EUR/GJ]"
label variable Erdgaspreis_HH_ct_kWh "consumer prices of households: natural gas [ct/kWh]"
label variable Heizoelpreis_HH_EUR_100l	"consumer prices of households: fuel oil [EUR/100l]"
label variable Strompreis_HH_ct_kWh	"consumer prices of households: electricity [ct/kWh]"
label variable Steinkohle_Import_EUR_t_SKE	"import prices: hard coal [EUR/t_SKE]"
label variable Erdgas_Import_EUR_TJ	"import prices: natural gas [EUR/TJ]"
label variable Rohoel_Import_EUR_t	"import prices: crude oil [EUR/t]"
label variable Steinkohle_Import_defl_BJ2015 "import price index for hard coal (baseyear 2015=100)"
label variable Erdgas_Import_defl_BJ2015	"import price index for natural gas (baseyear 2015=100)"
label variable Erdgas_defl_BJ2015	"producer price index for natural gas, liquefied or gaseous (baseyear 2015=100)"
label variable Erdoel_defl_BJ2015	"price index for crude oil from bituminous minerals (baseyear 2015=100)"
label variable Braunkohle_defl_BJ2015	"price index for lignite (baseyear 2015=100)"
label variable Heizoel_l_HH_defl_BJ2015	"price index for light fuel oil for household consumption (baseyear 2015=100)"
label variable Heizoel_s_defl_BJ2010	"producer price index for heavy fuel oil (baseyear 2015=100)"
label variable Strom_defl_BJ2015		"price index for electricity (baseyear 2015=100)"
label variable Strom_Boerse_defl_BJ2015	"price index for electricity listed on the stock exchange (baseyear 2015=100)"
label variable Strom_Transm_defl_BJ2015	"price index for electricity transmission services (baseyear 2015=100)"
label variable Strom_Vtlg_defl_BJ2015	"price index for electricity distribution services (baseyear 2015=100)"
label variable Strom_Trade_defl_BJ2015	"price index for electricity trading services (baseyear 2015=100)"
label variable Strom_WVtlg_defl_BJ2015	"price index for electricity re-distributors (baseyear 2015=100)"
label variable Strom_HH_defl_BJ2015		"price index for electricity for household consumption (baseyear 2015=100)"
label variable Strom_Gewerbe_defl_BJ2015	"price index for electricity for commercial use (baseyear 2015=100)"
label variable Strom_SK_NS_defl_BJ2015	"price index for electricity for special-contract customers in low-voltage tariffs (baseyear 2015=100)"
label variable Strom_SK_HS_defl_BJ2015	"price index for electricity for special-contract customers in high-voltage tariffs (baseyear 2015=100)"
label variable FW_defl_BJ2015	"price index for district heat (baseyear 2015=100)"
label variable FW_MFH_defl_BJ2015	"price index for district heat, apartment building (baseyear 2015=100)"
label variable FW_NWG_defl_BJ2015	"price index for district heat, non-residential building (baseyear 2015=100)"
label variable Erdgas_all_defl_BJ2015	"price index for natural gas transmission, distribution and trade services (baseyear 2015=100)"
label variable Erdgas_HH_defl_BJ2015	"price index for natural gas for household consumption (baseyear 2015=100)"
label variable Erdgas_Ind_defl_BJ2015	"price index for natural gas for manufacturing (baseyear 2015=100)"
label variable Erdgas_HuG_defl_BJ2015	"price index for natural gas for trade and commercial use (baseyear 2015=100)"
label variable Erdgas_KW_defl_BJ2015	"price index for natural gas for power plants (baseyear 2015=100)"
label variable Erdgas_WVtlg_defl_BJ2015	"price index for natural gas for re-distributors (baseyear 2015=100)"
label variable Erdgas_Boerse_defl_BJ2015	"price index for natural gas listed on stock markets (baseyear 2015=100)"
label variable Wa_defl_BJ2015	"price index for water supply (baseyear 2015=100)"
label variable Wa_HH_defl_BJ2015	"price index for water for household consumption (10m3 per month) (baseyear 2015=100)"
label variable Wa_Ind_5000_BJ2015	"price index for water supplied to manufacturing (5000m3 per month) (baseyear 2015=100)"
label variable Wa_Ind_50000_BJ2015	"price index for water supplied to manufacturing (50000m3 per month) (baseyear 2015=100)"
label variable Wa_WVU_BJ2015	"price index for water supplied to re-distributors (baseyear 2015=100)"

rename Jahr year
drop if year==.

save "$MAINDIR\TEMP1.dta", replace

********************************************************************************
*			2) Load national account (VGR) data I
********************************************************************************

/* Source: 
Volkswirtschaftliche Gesamtrechnung des Bundes: Inlandsprodukt 2019.
*/

use "$MAINDIR\VGR-Data_1991-2019\VGR_Data_1991-2017.dta", clear

merge m:1 year using "$MAINDIR\TEMP1.dta"
drop _merge

save "$MAINDIR\TEMP1.dta", replace

********************************************************************************
*			3) Load national account (VGR) data II
********************************************************************************

/* Source: 
Volkswirtschaftliche Gesamtrechnung des Bundes: Inlandsprodukt 2019.
*/

* lifecycle of capital stocks
* ------------------------------
import excel "$MAINDIR\VGR-Data_1991-2019\VGR_Lifecycle_capitalstock_2003-2016.xlsx", ///
sheet("L_asset") firstrow clear

merge 1:m year using "$MAINDIR\TEMP1.dta"
drop _merge

label variable L_asset "average service life of assets from current investment year (VGR)"
save "$MAINDIR\TEMP1.dta", replace


* lifecycle of buildings
* ----------------------
import excel "$MAINDIR\VGR-Data_1991-2019\VGR_Lifecycle_capitalstock_2003-2016.xlsx", ///
sheet("L_build") firstrow clear

merge 1:m year using "$MAINDIR\TEMP1.dta"
drop _merge

label variable L_build "average service life of buildings from current investment year (VGR)"
save "$MAINDIR\TEMP1.dta", replace


* lifecycle of equipment
* ----------------------
import excel "$MAINDIR\VGR-Data_1991-2019\VGR_Lifecycle_capitalstock_2003-2016.xlsx", ///
sheet("L_equip") firstrow clear

merge 1:m year using "$MAINDIR\TEMP1.dta"
drop _merge

label variable L_equip "average service life of equipment from current investment year (VGR)"
save "$MAINDIR\TEMP1.dta", replace


* lifecycle of intellecutal property 
* -----------------------------------
import excel "$MAINDIR\VGR-Data_1991-2019\VGR_Lifecycle_capitalstock_2003-2016.xlsx", ///
sheet("L_other") firstrow clear

merge 1:m year using "$MAINDIR\TEMP1.dta"
drop _merge

label variable L_other "average service life of intellectual property from current investment year (VGR)"

save "$MAINDIR\TEMP1.dta", replace


********************************************************************************
*			4) Load national account (VGR) data III
********************************************************************************

/* Source: 
Volkswirtschaftliche Gesamtrechnung des Bundes: Inlandsprodukt 2019.
*/

* net assets evaluated at replacement costs (fixed assets)
* --------------------------------------------------------
import excel "$MAINDIR\VGR-Data_1991-2019\VGR_netassets_replacement_2003-2016.xlsx", ///
sheet("CS_asset") firstrow clear

merge 1:m year WZ_new_1 using "$MAINDIR\TEMP1.dta"
drop _merge

label variable CS_asset " net (fixed) assets evaluated at replacement costs (VGR)"

save "$MAINDIR\TEMP1.dta", replace


* net assets at replacement costs (equipment)
* -------------------------------------------
import excel "$MAINDIR\VGR-Data_1991-2019\VGR_netassets_replacement_2003-2016.xlsx", ///
sheet("CS_equip") firstrow clear

merge 1:m year WZ_new_1 using "$MAINDIR\TEMP1.dta"
drop _merge

label variable CS_equip "net assets (equipment) evaluated at replacement costs (VGR)"

save "$MAINDIR\TEMP1.dta", replace


* net assets at replacement costs (buildings)
* -------------------------------------------
import excel "$MAINDIR\VGR-Data_1991-2019\VGR_netassets_replacement_2003-2016.xlsx", ///
sheet("CS_build") firstrow clear

merge 1:m year WZ_new_1 using "$MAINDIR\TEMP1.dta"
drop _merge

label variable CS_build "net assets (buildings) evaluated at replacement costs (VGR)"

save "$MAINDIR\TEMP1.dta", replace



* net assets at replacement costs (other)
* --------------------------------------------
import excel "$MAINDIR\VGR-Data_1991-2019\VGR_netassets_replacement_2003-2016.xlsx", ///
sheet("CS_other") firstrow clear

merge 1:m year WZ_new_1 using "$MAINDIR\TEMP1.dta"
drop _merge

label variable CS_other "net assets (others) evaluated at replacement costs (VGR)"

save "$MAINDIR\TEMP1.dta", replace


********************************************************************************
*			5) Load Destatis data
********************************************************************************

/* Source: 
Destatis (2018): Erzeugerpreisindex gewerblicher Produkte Fachserie 17 Reihe 2.
*/

* Price index for investment goods (baseyear = 2010)
* --------------------------------------------------
import excel "$MAINDIR\VGR-Data_1991-2019\VGR_investmentgoodsindex_1991-2017.xlsx", ///
sheet("Daten") firstrow clear

merge 1:m year using "$MAINDIR\TEMP1.dta"
drop _merge

label variable def_ppi_cg "price index for investment goods with baseyear = 2010 (Destatis)"


save "$MAINDIR\TEMP1.dta", replace

********************************************************************************
*			6) Load Bundesbank data
********************************************************************************

/*
Sources:
Bundesbank (2020): Hochgerechnete Angaben aus Jahresabschlüssen deutscher 
Unternehmen von 1995 bis 2018.
Bundesbank: Zinssatz der EZB für Hauptrefinanzierungsgeschäfte / Stand am Monatsende. BBK01.SU0202
Bundesbank: Umlaufsrenditen inländischer Inhaberschuldverschreibungen / Gesamt / Monatswerte. 
BBSIS.M.I.UMR.RD.EUR.A.B.A.A.R.A.A._Z._Z.A 
Bundesbank: Umlaufsrenditen inländischer Inhaberschuldverschreibungen / Anleihen der öffentlichen Hand / Monatswerte
BBSIS.M.I.UMR.RD.EUR.S13.B.A.A.R.A.A._Z._Z.A
*/

* bonds and main refinancing rate (EZB)
* -------------------------------------
import excel "$MAINDIR\Bundesbank Data\Bundesbank_AllData_1991-2019.xlsx", ///
sheet("BundesbankData") firstrow clear

merge 1:m year using "$MAINDIR\TEMP1.dta", keepusing ()
drop _merge

label variable HRefEZB "Main refinancing rate of the ECB (yearly average) (Bundesbank)"
label variable Umlaufrendite_Gesamt "yields on bonds outstanding (all bearer bonds) (Bundesbank)"
label variable Umlaufrendite_Staat "yields on bonds outstanding (government bonds) (Bundesbank)"

save "$MAINDIR\TEMP1.dta", replace

* equity ratio
* ------------
import excel "$MAINDIR\Bundesbank Data\Bundesbank_AllData_1991-2019.xlsx", ///
sheet("BundesbankData_WZ") firstrow clear

merge 1:m year WZ_new_1 using "$MAINDIR\TEMP1.dta"
drop _merge

label variable EKQ "equity ratio (equity in % of total assets) (Bundesbank)"
save "$MAINDIR\TEMP1.dta", replace


********************************************************************************
* 			5) Federal Network Agency (BNetzA) Data
********************************************************************************

* Sources: BNetzA 2008: Beschluss BK4-08-068.

* equity risk premium
* -------------------
import excel "$MAINDIR\BNetzA Data\BNetzA_risk_factor_and_equity_risk_premium.xlsx", ///
sheet("erp") firstrow clear

merge 1:m WZ_new_1 using "$MAINDIR\TEMP1.dta"
drop _merge

label variable erp "equity risk premium for electricity DSOs (BNetzA)"
save "$MAINDIR\TEMP1.dta", replace


* beta
* -------------------
import excel "$MAINDIR\BNetzA Data\BNetzA_risk_factor_and_equity_risk_premium.xlsx", ///
sheet("beta") firstrow clear

merge 1:m WZ_new_1 using "$MAINDIR\TEMP1.dta"
drop _merge

label variable beta "individual risk factor for capital asset pricing model (BNetzA)"
save "$MAINDIR\TEMP1.dta", replace



********************************************************************************
* 				Clean and save
********************************************************************************

* clean
cap noi drop D E F

label variable WZ_new_1 "NACE Rev. 2 1-digit"

order year WZ_new_1 WZ_new_2
sort year WZ_new_1 WZ_new_2

* clean and save
save "$MAINDIR\4193_ExterneWirtschaftsdaten.dta", replace

cap noi erase "$MAINDIR\TEMP1.dta"

cap log close
********************************************************************************
* 				End of file
*********************************************************************************
